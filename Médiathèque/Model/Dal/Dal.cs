﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Médiathèque.Model.Dal
{
    abstract class Dal
    {
        private string server = "127.0.0.1";
        private string port = "3306";
        private string dbname = "mediatheque";
        private string charset = "utf8";
        private string username = "mediatheque";
        private string password = "K5RhCrjWvOmBk2ZS";

        private string connectionString
        {
            get
            {
                StringBuilder str = new StringBuilder();
                str.Append("Server=" + server + ";");
                str.Append("Port=" + port + ";");
                str.Append("Database=" + dbname + ";");
                str.Append("Charset=" + charset + ";");
                str.Append("Username=" + username + ";");
                str.Append("Password=" + password + ";");
                return str.ToString();
            }
        }

        protected MySqlConnection Connection;

        protected Dal()
        {
            Connection = new MySqlConnection();
            Connection.ConnectionString = connectionString;
        }
    }
}
