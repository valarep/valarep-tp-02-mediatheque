﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Médiathèque.Model.Dal;
using Médiathèque.Model.Classes;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Médiathèque.Model.Dao
{
    class OuvrageDao : Dal.Dal
    {
        public OuvrageDao() : base() { }

        public List<Ouvrage> GetAll()
        {
            List<Ouvrage> ouvrages = new List<Ouvrage>();

            string cmdText = @"SELECT *
                               FROM `ouvrage`; ";

            try
            {
                Connection.Open();
                MySqlCommand command = Connection.CreateCommand();
                command.CommandText = cmdText;
                command.Prepare();
                // Paramétrage ?
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string isbn = reader.GetString("isbn");
                    string titre = reader.GetString("titre");
                    string description = reader.GetString("description");
                    Ouvrage ouvrage = new Ouvrage()
                    {
                        Isbn = isbn,
                        Titre = titre,
                        Description = description
                    };
                    ouvrages.Add(ouvrage);
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erreur d'accès à la base de données.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.Message);
            }

            if (Connection.State == System.Data.ConnectionState.Open)
            {
                Connection.Close();
            }
            return ouvrages;
        }

        public int Update(string isbn, string titre, string description)
        {
            int nbRows;

            string cmdText = @"UPDATE `ouvrage`
                               SET
                                    `titre` = ?titre,
                                    `description` = ?description
                               WHERE `isbn` = ?isbn;
                                ";
            try
            {
                Connection.Open();
                MySqlCommand command = Connection.CreateCommand();
                command.CommandText = cmdText;
                command.Prepare();
                // Paramétrage ?
                command.Parameters.Add(new MySqlParameter("titre", titre));
                command.Parameters.Add(new MySqlParameter("description", description));
                command.Parameters.Add(new MySqlParameter("isbn", isbn));
                nbRows = command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erreur d'accès à la base de données.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.Message);
                nbRows = -1;
            }

            if (Connection.State == System.Data.ConnectionState.Open)
            {
                Connection.Close();
            }

            return nbRows;
        }
    }
}
