﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Médiathèque.Model.Classes
{
    class Ouvrage
    {
        public string Isbn { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Titre;
        }
    }
}
