﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Médiathèque.Model.Dao;
using Médiathèque.Model.Classes;

namespace Médiathèque
{
    public partial class Form1 : Form
    {
        List<Ouvrage> ouvrages;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OuvrageDao dao = new OuvrageDao();
            ouvrages = dao.GetAll();
            lstOuvrages.DataSource = ouvrages;
        }

        private void lstOuvrages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstOuvrages.DataSource != null)
            {
                Ouvrage ouvrage = (Ouvrage)lstOuvrages.SelectedItem;
                txtIsbn.Text = ouvrage.Isbn;
                txtTitre.Text = ouvrage.Titre;
                txtDescription.Text = ouvrage.Description;
            }
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {

        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            string isbn = txtIsbn.Text;
            string titre = txtTitre.Text;
            string description = txtDescription.Text;

            OuvrageDao dao = new OuvrageDao();
            int nbRows = dao.Update(isbn, titre, description);

            if (nbRows == 1)
            {
                Ouvrage ouvrage = (Ouvrage)lstOuvrages.SelectedItem;

                ouvrage.Isbn = isbn;
                ouvrage.Titre = titre;
                ouvrage.Description = description;

                lstOuvrages.DataSource = null;
                lstOuvrages.DataSource = ouvrages;
            }

        }

        private void Supprimer_Click(object sender, EventArgs e)
        {

        }
    }
}
