﻿namespace Médiathèque
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstOuvrages = new System.Windows.Forms.ListBox();
            this.lblIsbn = new System.Windows.Forms.Label();
            this.grpOuvrage = new System.Windows.Forms.GroupBox();
            this.txtIsbn = new System.Windows.Forms.MaskedTextBox();
            this.lblTitre = new System.Windows.Forms.Label();
            this.txtTitre = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.btnModifier = new System.Windows.Forms.Button();
            this.Supprimer = new System.Windows.Forms.Button();
            this.grpOuvrage.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstOuvrages
            // 
            this.lstOuvrages.FormattingEnabled = true;
            this.lstOuvrages.Location = new System.Drawing.Point(12, 12);
            this.lstOuvrages.Name = "lstOuvrages";
            this.lstOuvrages.Size = new System.Drawing.Size(120, 420);
            this.lstOuvrages.TabIndex = 0;
            this.lstOuvrages.SelectedIndexChanged += new System.EventHandler(this.lstOuvrages_SelectedIndexChanged);
            // 
            // lblIsbn
            // 
            this.lblIsbn.AutoSize = true;
            this.lblIsbn.Location = new System.Drawing.Point(6, 22);
            this.lblIsbn.Name = "lblIsbn";
            this.lblIsbn.Size = new System.Drawing.Size(38, 13);
            this.lblIsbn.TabIndex = 1;
            this.lblIsbn.Text = "ISBN :";
            // 
            // grpOuvrage
            // 
            this.grpOuvrage.Controls.Add(this.txtDescription);
            this.grpOuvrage.Controls.Add(this.lblDescription);
            this.grpOuvrage.Controls.Add(this.txtTitre);
            this.grpOuvrage.Controls.Add(this.lblTitre);
            this.grpOuvrage.Controls.Add(this.txtIsbn);
            this.grpOuvrage.Controls.Add(this.lblIsbn);
            this.grpOuvrage.Location = new System.Drawing.Point(138, 12);
            this.grpOuvrage.Name = "grpOuvrage";
            this.grpOuvrage.Size = new System.Drawing.Size(346, 171);
            this.grpOuvrage.TabIndex = 2;
            this.grpOuvrage.TabStop = false;
            this.grpOuvrage.Text = "Ouvrage sélectionné";
            // 
            // txtIsbn
            // 
            this.txtIsbn.Location = new System.Drawing.Point(78, 19);
            this.txtIsbn.Mask = "0000000000000";
            this.txtIsbn.Name = "txtIsbn";
            this.txtIsbn.Size = new System.Drawing.Size(100, 20);
            this.txtIsbn.TabIndex = 2;
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Location = new System.Drawing.Point(6, 48);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(34, 13);
            this.lblTitre.TabIndex = 3;
            this.lblTitre.Text = "Titre :";
            // 
            // txtTitre
            // 
            this.txtTitre.Location = new System.Drawing.Point(78, 45);
            this.txtTitre.Name = "txtTitre";
            this.txtTitre.Size = new System.Drawing.Size(262, 20);
            this.txtTitre.TabIndex = 4;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(6, 74);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(66, 13);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Text = "Description :";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(78, 71);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(262, 94);
            this.txtDescription.TabIndex = 6;
            // 
            // btnAjouter
            // 
            this.btnAjouter.Location = new System.Drawing.Point(490, 17);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(75, 23);
            this.btnAjouter.TabIndex = 3;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(490, 46);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(75, 23);
            this.btnModifier.TabIndex = 4;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // Supprimer
            // 
            this.Supprimer.Location = new System.Drawing.Point(490, 75);
            this.Supprimer.Name = "Supprimer";
            this.Supprimer.Size = new System.Drawing.Size(75, 23);
            this.Supprimer.TabIndex = 5;
            this.Supprimer.Text = "Supprimer";
            this.Supprimer.UseVisualStyleBackColor = true;
            this.Supprimer.Click += new System.EventHandler(this.Supprimer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Supprimer);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.btnAjouter);
            this.Controls.Add(this.grpOuvrage);
            this.Controls.Add(this.lstOuvrages);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpOuvrage.ResumeLayout(false);
            this.grpOuvrage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstOuvrages;
        private System.Windows.Forms.Label lblIsbn;
        private System.Windows.Forms.GroupBox grpOuvrage;
        private System.Windows.Forms.MaskedTextBox txtIsbn;
        private System.Windows.Forms.TextBox txtTitre;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Button Supprimer;
    }
}

